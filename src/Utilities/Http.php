<?php namespace Ldynia\Core\Utilities;

class Http {

    private static $instance;

    public function __construct() {

    }

    public static function get($url= '', $queryData = [], $header = []) {
        if (isset($queryData)) {
            $url = $url .'?' . http_build_query($queryData);
        };
        return self::curl('GET', $url);
    }

    public static function post($url= '', $postFields = [], $header = [], $credentials =[]) {
        return self::curl('POST', $url, $postFields, $header, $credentials);
    }

    private static function curl($request = 'GET', $url = null, $postFields = [], $header = [], $credentails = []) {
        try {

            $curl = curl_init($url); // execute GET request
            if (array_key_exists('username', $credentails) AND array_key_exists('password', $credentails)) {
                $cr = $credentails['username'] . ":" . $credentails['password'];
                curl_setopt($curl, CURLOPT_USERPWD, $cr);
            }

            if ($request == 'POST') {
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
            }

            if (!empty($header)) {
                curl_setopt($curl, CURLINFO_HEADER_OUT, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            }

            curl_setopt($curl, CURLOPT_VERBOSE, true);
            curl_setopt($curl, CURLOPT_HEADER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $response = curl_exec($curl);
            $responseJson = json_decode($response);
            if ($responseJson == null) {
                $indexOfOpeningBracker = strpos($response, "{");
                $indexOfClosingBracker = strpos($response, "}");
                $jsonString = substr($response, $indexOfOpeningBracker, $indexOfClosingBracker);
                $responseJson = json_decode($jsonString);
            }
            
            $requestInfo = curl_getinfo($curl);
            curl_close($curl);

            $resonseCode = $requestInfo['http_code'];
            $responseData = [
                'code' => $resonseCode,
                'response' => $responseJson
            ];

            return $responseData;

        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }

}
