<?php namespace Ldynia\Core\Model;

use Illuminate\Validation\Factory as Validator;
use Ldynia\Core\Exceptions\FormValidationException;

abstract class FormValidator {

	private $validator;
	private $validation;

	public function __construct(Validator $validator) {
		$this->validator = $validator;
	}

	public function validateCustom(array $formData, $customRules = [], $customMessage = []) {
	  $validationRules = !empty($customRules) ? $customRules : $this->getValidationRules();
	  $validationMessage = !empty($customMessage) ? $customMessage : $this->getValidationMessages();
	  $this->validation = $this->validator->make(
	    $formData,
	    $customRules,
	    $validationMessage
	  );
	  $this->guardFailedValidation();
	}

	public function validate(array $formData, $rulesModifier = []) {
		$rules = $this->getValidationRules();
		if (!empty($rulesModifier)) {
			$rules = $this->merge_rules($rules, $rulesModifier);
		}
		$this->data = $formData;
		$this->validation = $this->validator->make(
			$formData,
			$rules,
			$this->getValidationMessages()
		);
		$this->guardFailedValidation();
	}

	public function validateUpdate(array $formData, $rulesModifier = []) {
		$rules = $this->getValidationUpdateRules();
		if (!empty($rulesModifier)) {
			$rules = $this->merge_rules($rules, $rulesModifier);
		}
		$this->data = $formData;
		$this->validation = $this->validator->make (
			$formData,
			$rules,
			$this->getValidationMessages()
		);
		$this->guardFailedValidation();
	}

	private function merge_rules($origianl, $new) {
		return array_merge($origianl, $new);
	}

	private function guardFailedValidation() {
		if ($this->validation->fails()) {
			throw new FormValidationException('Validation failed', $this->getValidationErrors());
		}
	}

	private function getValidationRules() {
		return $this->rules;
	}

	private function getValidationUpdateRules() {
		return $this->updateRules;
	}

	private function getValidationErrors() {
		return $this->validation->errors();
	}

	private function getValidationMessages() {
		return isset($this->messages) ? $this->messages : [];
	}

}
