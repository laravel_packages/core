<?php

namespace Ldynia\Core\Utilities;

class Helper
{

    public static function arrayToString($dataArray = []) {
        $arrayString = json_encode($dataArray);
        $arrayStringReadable = str_replace(["{", "}", "[", "]"], "", $arrayString);
        $arrayStringReadable = str_replace(",", "\r\n", $arrayStringReadable);
        return $arrayStringReadable . "\r\n";
    }

    public static function passwordGenerator($length = 4, $face = 'lower', $type = 'string')
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        if ($type == 'digits') {
            $chars = "0123456789";
        }
        $chars = ($face == 'lower') ? strtolower($chars) : $chars;
        return substr(str_shuffle($chars), 0, $length);
    }

    public static function inputNormalizer($input = [])
    {
        foreach ($input as $key => $value) {
            if (in_array($value, ['true', 'True', 'TRUE', 'false', 'FALSE', 'False'])) {
                $input[$key] = self::booleanNormalizer($value);
            }
        }
        return $input;
    }

    private static function booleanNormalizer($value)
    {
        return in_array($value, ['true', 'True', 'TRUE']);
    }
}
