<?php namespace Ldynia\Core\Assets;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Assets {

	public static function add($file, $filename = null, $disk = 's3') {
		
		if (!isset($file) OR empty($file)) {
			throw new \Exception("Stop moron a file cannot be empty!!!");
		}

		if (env('APP_NAME') == null) {
			throw new \Exception("Could not upload image due to missing APP_NAME in .env file");
		}

		if ($filename == null) {
			$date = new \DateTime();
			$fileName = uniqid() . '_' . $date->getTimestamp();
			$extention = $file->getClientOriginalExtension();
			if ($extention != '') {
				$fileName = $fileName . '.' . $file->getClientOriginalExtension();
			}
		}

		$directory = env('APP_NAME') . '/' . self::generateMimeBaseDirectory($file->getMimeType());
		$path = $directory  . '/' .  self::stringNormalizer($fileName);

		
		$fileUploaded = Storage::disk($disk)->put($path, File::get($file), 'public');
		if (!$fileUploaded) {
			throw new \Exception("Failed to upload file.");
		}

		return self::generateResoureceUrl($path, $disk);
	}

	static function generateResoureceUrl($path, $disk) {
		$bucket = (env('APP_ENV') == 'local') ? env('AWS_BUCKET_LOCAL') : env('AWS_BUCKET_PRODUCTION');
		return Storage::disk($disk)->getDriver()->getAdapter()->getClient()->getObjectUrl($bucket, $path);
	}

	static function stringNormalizer($string) {
		return preg_replace('/[^a-z0-9_\-.]/ui', '', $string);	
	}

	static function generateMimeBaseDirectory($mimeType, $directory = 'assets/others') {
		if (strstr($mimeType, 'image')) {
			return 'assets/images';
		}
		if (strstr($mimeType, 'video')) {
			return 'assets/videos';
		}
		if (strstr($mimeType, 'audio')) {
			return 'assets/audios';
		}
		if (strstr($mimeType, 'text')) {
			return 'assets/texts';
		}
		if (strstr($mimeType, 'pdf')) {
			return 'assets/pdfs';
		}
		return $directory;
	}
		
}