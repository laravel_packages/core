<?php

namespace Ldynia\Core\Notifications\Push;

use Exception;
use InvalidArgumentException;
use Ldynia\Core\Utilities\Http;

/**
 * API documentation cab be found under the link
 *
 * @link http://docs.urbanairship.com/api/ua.html#audience-selectors
 */
class UrbanAirship
{
    private $appKey;
    private $masterSecret;
    private $pushEndpoint = 'https://go.urbanairship.com/api/push/';

    public function __construct()
    {
        if ($this->hasValidKeys()) {
            $this->appKey = env('URBAN_AIRSHIP_APP_KEY');
            $this->masterSecret = env('URBAN_AIRSHIP_MASTER_SECRET');
        }
    }

    public function push($settings = [])
    {
        $data = [];
        $header = [
            'Content-Type: application/json;',
            'Accept: application/vnd.urbanairship+json; version=3;',
        ];
        $credentials = ['username' => $this->appKey, 'password' => $this->masterSecret];

        // send push to all else send message to individual user
        if (array_key_exists('audience', $settings) and $settings['audience'] == 'all') {
            $data['audience'] = 'all';
            $data['device_types'] = 'all';
        } else {
            if (!array_key_exists('device_types', $settings)) {
                if (is_array($settings['device_types'])) {
                    if (in_array(['all', 'android', 'ios', 'amazon'], $settings['device_types'])) {
                        throw new InvalidArgumentException('UrbanAirship is missing device_types or provided type is not supported');
                    }
                }
                if (!in_array($settings['device_types'], ['all', 'android', 'ios', 'amazon'])) {
                    throw new InvalidArgumentException('UrbanAirship is missing device_types or provided type is not supported');
                }
            }
            $data['device_types'] = $settings['device_types'];

            if (!array_key_exists('channel_type', $settings) or !in_array($settings['channel_type'], ['ios_channel', 'android_channel', 'amazon_channel'])) {
                throw new InvalidArgumentException('UrbanAirship is missing channel_type or provided type is not supported');
            }
            
            if (array_key_exists('channel_id', $settings)) {
                $data['audience'][$settings['channel_type']] = $settings['channel_id'];
            }

            if (array_key_exists('named_user', $settings)) {
                $data['audience']['named_user'] = $settings['named_user'];
            }
        }

        $data['notification']['alert'] = !empty($settings['alert']) ? $settings['alert'] : "This is test message";

        return Http::post($this->pushEndpoint, json_encode($data), $header, $credentials);
    }

    private function hasValidKeys()
    {
        if (empty(env('URBAN_AIRSHIP_APP_KEY'))) {
            throw new Exception("URBAN_AIRSHIP_APP_KEY is missing from environment file");
        }
        if (empty(env('URBAN_AIRSHIP_MASTER_SECRET'))) {
            throw new Exception("URBAN_AIRSHIP_MASTER_SECRET is missing from environment file");
        }
        return true;
    }
}
