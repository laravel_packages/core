# Core Package

The purpose of the package is to facilitates common operations encauntered in application development. So far package contains following Classess 

  - Repository
  
### Version 
0.1

### Tech

Laravel 5

### Installation
```sh
$ composer require ldynia/core
```

### Configuration 

Because at this moment I have problem with fixing ServiceProvider. I am enforced to load package according to psr-4 standard (nampsaceing). File composer.json

```php
"autoload": {
    "psr-4": {
        "Ldynia\\Core\\": "vendor/ldynia/core/src"
    }
}
```

Add Service provider to providers array in config/app.php

```php
// laravel 5.0
'Ldynia\Core\CoreServiceProvider'

// laravel 5.1
Ldynia\Core\CoreServiceProvider::class

````

[Me Website]:http://me.coding-error.com
