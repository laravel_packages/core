<?php namespace Ldynia\Core\Exceptions;

use Illuminate\Support\MessageBag;

class FormValidationException extends \Exception {

	private $errors;

	public function __construct($message, MessageBag $errors) {
		parent::__construct($message);
		$this->errors = $errors;
	}

	public function getErrors() {
		return $this->errors;
	}

}