<?php namespace Ldynia\Core;

use Illuminate\Support\ServiceProvider;

class CoreServiceProvider extends ServiceProvider {

	/**
	* Bootstrap the application services.
	*
	* @return void
	*/
	public function boot()
	{
		$path = base_path() . '/vendor/autoload.php';
		require_once($path);
	}


	/**
	* Register the application services.
	*
	* @return void
	*/
	public function register()
	{
		//
	}
}