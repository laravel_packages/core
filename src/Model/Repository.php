<?php namespace Ldynia\Core\Model;

use Ldynia\Core\Exceptions\ModelNotFoundException;

abstract class Repository {
	
	protected $model;

	public function __construct($model) {
		$this->model = $model;
	}

	public function getModel() {
		return $this->model;
	}

	public function findAll($fields = ['*']) {
		return $this->model->all($fields);
	}

	public function findById($id) {
		if ($model = $this->model->find($id)) {
			return $model;
		}
		$this->throwModelNotFoundException();
	}

	public function findOrFail($id) {
		return $this->model->findOrFail($id);
	}

	public function findByKeyValue($keyArray = [], $valueArray =[], $valueOperator  = '=', $keyOperator  = '=') {
		$keyName = current(array_keys($keyArray)); 
		$keyValue = current(array_values($keyArray)); 
		$valueName = current(array_keys($valueArray)); 
		$valueValue = current(array_values($valueArray)); 
		return $this->model->where($keyName, $keyOperator, $keyValue)->where($valueName, $valueOperator, $valueValue);
	}

	public function findWhere($field, $value, $operator = '=') {
		return $this->model->where($field, $operator, $value);
	}

	public function orWhere($field, $operator = '=', $value) {
		return $this->model->orWhere($field, $operator, $value);
	}

	public function search($field, $query) {
		return $this->model->where($field, 'LIKE', '%' . $query . '%');
	}

	public function orderAndPaginate($field, $order = 'asc', $count = 10) {
		$field = empty($field) ? 'created_at' : $field;
		$count = abs($count) <= 100 ? $count : 10;
		return $this->model->orderBy($field, $order)->paginate($count);
	}

	public function orderBy($field, $order = 'asc') {
		return $this->model->orderBy($field, $order);
	}

	public function delete($id) {
		return $this->model->findOrFail($id)->delete();
	}

	public function paginate($count = 10) {
		$count = abs($count) <= 100 ? $count : 10;
		return $this->model->paginate($count);
	}

	public function save($data) {
		if ($data instanceOf Model) {
			return $this->saveModel($data);
		}
		$model = $this->arrayToModel($data);
		return $this->saveModel($model);
	}

	public function create($data) {
		if ($data instanceOf Model) {
			throw new Exception("Invalid input given. It has to be an array not Instance of Model (object)");
		}
		return $this->model->create($data);
	}

	public function update($data) {
		if (is_array($data)) {
			$id = isset($data['_id']) ? $data['_id'] : $data['id']; 
			$model = $this->findOrFail($id);
			return $model->update($data);
		}
		return $this->updateModel($data);
	}

	private function saveModel($model) {
		if ($model->getDirty()) {
			return $model->save();
		}
		return $model->touch();
	}

	private function updateModel($model) {
		if ($model->getDirty()) {
			return $model->update();
		}
		return $model->touch();
	}

	public function arrayToModel($data = []) {
		return $this->model->newInstance($data);
	}

	private function throwModelNotFoundException() {
		$modelName = class_basename($this->model);
		throw new ModelNotFoundException("{$modelName} not found", 404);
	}
	
}