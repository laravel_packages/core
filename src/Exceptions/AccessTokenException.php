<?php namespace Ldynia\Core\Exceptions;

use \Symfony\Component\HttpKernel\Exception\HttpException;

class AccessTokenException extends HttpException {

	public function __construct($message, $code, $param = null, $headers = []) {
		parent::__construct($code, $message, $param, $headers);
	}

}