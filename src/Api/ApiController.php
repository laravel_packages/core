<?php 

namespace Ldynia\Core\Api;

use Closure;
use \Illuminate\Support\Facades\Response;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\ResourceInterface;
use League\Fractal\Pagination\PaginatorInterface;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Serializer\JsonApiSerializer;
use Illuminate\Http\Request;
use Ldynia\Core\Api\ApiAuthMiddleware;

class ApiController extends Controller
{
    private $statusCode = 200;
    private $fractalManager;

    public function __construct()
    {
        $this->fractalManager = new Manager;
        $this->fractalManager->setSerializer(new JsonApiSerializer());
    }

    public function getManager()
    {
        return $this->fractalManager;
    }

    public function responseWithSuccess($message = '')
    {
        $this->statusCode = 200;
        return $this->response(['success' => true, 'status_code' => 200, 'message' => $message]);
    }

    public function responseWithError($message, $statusCode = 400)
    {
        $this->statusCode = $statusCode;

        $data = [
            'success'     => false,
            'status_code' => $this->statusCode,
            'message' => $message
        ];

        return $this->response($data);
    }

    public function responseWithItem($item, TransformerAbstract $transformer, $namespace = 'data', Closure $callback = null)
    {
        $resource = new Item($item, $transformer, $namespace);
        
        if (!is_null($callback)) {
            call_user_func($callback, $resource);
        }

        return $this->response($resource);
    }

    public function responseWithCollection($items, TransformerAbstract $transformer, $namespace = 'data', Closure $callback = null)
    {
        $resources = new Collection($items, $transformer, $namespace);
        
        if (!is_null($callback)) {
            call_user_func($callback, $resources);
        }

        return $this->response($resources);
    }

    public function responseWithPagination($paginatedCollection, TransformerAbstract $transformer, $namespace = 'data', Closure $callback = null)
    {
        $resources = new Collection($paginatedCollection, $transformer, $namespace);
        
        if (!is_null($callback)) {
            call_user_func($callback, $resources);
        }

        $data = $this->collectionToArray($resources);
        $data['paginator'] = [
            "total_itmes" => $paginatedCollection->total(),
            "items_per_page" => $paginatedCollection->perPage(),
            "current_page" => $paginatedCollection->currentPage(),
            "last_page" => $paginatedCollection->lastPage(),
            "next_page_url" => $paginatedCollection->nextPageUrl(),
            "prev_page_url" => $paginatedCollection->previousPageUrl(),
        ];

        return $this->response($data);
    }

    public function response($data, array $headers = [])
    {
        if (!is_array($data)) {
            $data = $this->collectionToArray($data);
        }

        return Response::make($data, $this->statusCode, $headers);
    }

    private function collectionToArray($collection)
    {
        return $this->fractalManager->createData($collection)->toArray();
    }
}
