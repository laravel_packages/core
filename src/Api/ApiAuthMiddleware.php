<?php 

namespace Ldynia\Core\Api;
use DB;
use Auth;
use Closure;
use Ldynia\Core\Exceptions\AccessTokenException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use \Illuminate\Support\Facades\Response;
use App\Model\Frontend\User\User;

/**
 * Authentication Middleware base on the notion of the Frontend User.
 * YOU HAVE TO HAVE 'frontend_users' table/collection in your database.
 */
class ApiAuthMiddleware {


	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{	
		$authentication = $this->authenticate($request, $next, $authenticateOnRequest = true);
		if ($authentication['authenticated']) {
			return $next($request);
		}

		// return json response before the controller is hit back.
		return Response::make([
				'success' => false, 
				'status_code' => $authentication['status_code'], 
				'message' => $authentication['error']
			], 
			$authentication['status_code'], 
			array()
		);
	}

	private function authenticate($request, $next, $authenticateOnRequest) {
		try {
			if ($request->query->has('access_token')) {
				$key = 'access_token';
				$method = 'query';
				$accessToken = $request->query->all()[$key];
			}

			if ($request->headers->has('x-authenticate')) {
				$key = 'x-authenticate';
				$method = 'headers';
				$accessToken = $request->headers->all()[$key][0];
			}

			if (empty($accessToken)) abort(401, 'Unauthorized error. Missing access token.');

			$user = DB::collection('frontend_users')->where('access_token', $accessToken)->get();
			if (isset($user[0]['access_token_expires_at'])) {
				$tokenHasExpired = (time() > $user[0]['access_token_expires_at']->sec);
				if ($tokenHasExpired) abort(401, "Access token has expired.");
			}

			if (empty($user)) abort(403, 'Unauthorized action for this access token.');
			if (sizeof($user) > 1) abort(401, 'Some shit happened access token matches many users.');
			
			// login user automatically when access token matches
			if ($authenticateOnRequest) {
				if (strpos($request->route()->uri(), "api") > -1) {
					$auth = Auth::frontendUser();
					$userToModel = new User;
					foreach ($user[0] as $key => $value) {
						$userToModel->{$key} = $value;
					}
					$auth->login($userToModel);
				}
			}

			return ['authenticated' => true];
		} catch (HttpException $e) {
			return ['authenticated' => false, 'status_code' => $e->getStatusCode(), 'error' => $e->getMessage()];
		} catch (NotFoundHttpException $e) {
			return ['authenticated' => false, 'status_code' => $e->getStatusCode(), 'error' => $e->getMessage()];
		}
	}

}